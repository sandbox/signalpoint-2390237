<?php

/**
 * Determines whether the current user can access a kaltura resource.
 *
 * @param string $perm
 *   String indicating which permission to check access for.
 *
 * @return boolean
 *   Boolean indicating whether or not the user has access to the resource.
 */
function _services_kaltura_access($perm) {
  return user_access($perm);
}

/**
 *
 */
function services_kaltura_session_start() {
  $partnerId = variable_get('kaltura_partner_id', '');
  $config = new KalturaConfiguration($partnerId);
  $config->serviceUrl = variable_get('kaltura_server_url', '');
  $client = new KalturaClient($config);
  $secret = variable_get('kaltura_admin_secret', '');
  $userId = null;
  $type = KalturaSessionType::ADMIN;
  $expiry = null;
  $privileges = null;
  $result = $client->session->start($secret, $userId, $type, $partnerId, $expiry, $privileges);
  return $result;
}

