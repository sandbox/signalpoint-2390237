Setup
=====

1. Download and enable the module as usual
2. Go to: admin/structure/services
3. Click 'edit resources' next to your service resource, or create a new one
4. Enable the 'session_start' service resource under 'kaltura'
5. Click save
6. Flush all caches

Usage
=====

# session start
POST => http://www.example.com/endpoint/kaltura/session_start.json

