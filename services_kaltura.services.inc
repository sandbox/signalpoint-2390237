<?php

function services_kaltura_services_resources() {
  $resources = array(
    'kaltura' => array(
      'actions' => array(
        'session_start' => array(
          'help' => t('Starts a Kaltura session.'),
          'file' => array(
            'type' => 'inc',
            'module' => 'services_kaltura',
            'name' => 'services_kaltura.resource',
          ),
          'callback' => 'services_kaltura_session_start',
          'args' => array(),
          'access callback' => '_services_kaltura_access',
          'access callback file' => array(
            'type' => 'inc',
            'module' => 'services_kaltura',
            'name' => 'services_kaltura.resource',
          ),
          'access arguments' => array('services kaltura access'),
          'access arguments append' => TRUE,
        ),
      ),
    ),
  );
  return $resources;
}

